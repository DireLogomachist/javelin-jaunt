﻿using UnityEngine;
using System.Collections;

public class level_markers : MonoBehaviour {
	private static level_markers _instance;
    public static level_markers Instance { get { return _instance; } }

	public void Awake() {
		if (_instance != null && _instance != this) {
			Destroy(this.gameObject);
		} else {
			_instance = this;
		}
	}

	void Start () {}

	public void showLevelUI() {
		StartCoroutine(fadeInMarkers());
	}

	public void levelCompleteUI(int level) {
		if(level < 3)
			transform.GetChild(level).GetComponent<Animator>().Play("marker_center_fade_in");
	}

	IEnumerator fadeInMarkers() {
		yield return new WaitForSeconds(.5f);
		transform.GetChild(0).GetComponent<Animator>().Play("marker_ring_fade_in");
		yield return new WaitForSeconds(.5f);
		transform.GetChild(1).GetComponent<Animator>().Play("marker_ring_fade_in");
		yield return new WaitForSeconds(.5f);
		transform.GetChild(2).GetComponent<Animator>().Play("marker_ring_fade_in");
	}
}
