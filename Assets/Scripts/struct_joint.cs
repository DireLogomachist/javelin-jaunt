﻿using UnityEngine;
using System.Collections;
using UnityEngine.Serialization;

public class struct_joint : MonoBehaviour {
	public GameObject jointShard;

	public float hitFlashTimer = 0.5f;

	Renderer rend;
	Color currentColor;

	public ConfigurableJoint up;
	public ConfigurableJoint down;
	public ConfigurableJoint right;
	public ConfigurableJoint left;
	public ConfigurableJoint forward;
	public ConfigurableJoint back;

	public bool destructEnabled = false;

	[HideInInspector] public ConfigurableJoint[] allJoints;

	public Vector3 indexLoc;

	void Awake() {
		rend = gameObject.GetComponent<Renderer>();
		currentColor = rend.material.color;

		float t = Time.realtimeSinceStartup;

		ConfigurableJoint defaultJoint = gameObject.GetComponent<ConfigurableJoint>();
		utilities.CopyComponent(defaultJoint, gameObject);
		utilities.CopyComponent(defaultJoint, gameObject);
		utilities.CopyComponent(defaultJoint, gameObject);
		utilities.CopyComponent(defaultJoint, gameObject);
		utilities.CopyComponent(defaultJoint, gameObject);

		allJoints = gameObject.GetComponents<ConfigurableJoint>();
		up = allJoints[0];
		down = allJoints[1];
		right = allJoints[2];
		left = allJoints[3];
		forward = allJoints[4];
		back = allJoints[5];

		indexLoc = new Vector3(-1,-1,-1);

		destructEnabled = false;
	}

	void Start() {}
	void Update() {}

	void OnCollisionEnter(Collision c) {
		if((c.collider.tag == "Projectile") && (destructEnabled == true)) {
			selfDestruct();
		} else if((c.impulse.magnitude > 5.0f) && (destructEnabled == true)) {
			selfDestruct();
		}
	}

    IEnumerator colorChange() {
		rend.material.color = Color.red;
		yield return new WaitForSeconds(hitFlashTimer);
		rend.material.color = currentColor;
	}

	public void setIndex(Vector3 i) {
		indexLoc = i;
	}

	public void setIndex(IntVector3 i) {
		indexLoc = new Vector3(i.x,i.y,i.z);
	}

	public ConfigurableJoint selectConfigJoint(IntVector3 input) {
		if(input == new IntVector3(0,1,0)) {
			return up;
		} else if(input == new IntVector3(0,-1,0)) {
			return down;
		} else if(input == new IntVector3(-1,0,0)) {
			return left;
		} else if(input == new IntVector3(1,0,0)) {
			return right;
		} else if(input == new IntVector3(0,0,1)) {
			return forward;
		} else if(input == new IntVector3(0,0,-1)) {
			return back;
		} else {
			Debug.Log("Invalid selectConfigJoint input");
			return null;
		}
	}

	public void enableConfigJoint(ConfigurableJoint configJoint) {
		configJoint.xMotion = ConfigurableJointMotion.Limited;
		configJoint.yMotion = ConfigurableJointMotion.Limited;
		configJoint.zMotion = ConfigurableJointMotion.Limited;
		configJoint.angularXMotion = ConfigurableJointMotion.Limited;
		configJoint.angularYMotion = ConfigurableJointMotion.Limited;
		configJoint.angularZMotion = ConfigurableJointMotion.Limited;
	}

	public void selfDestruct() {
		if(gameObject != null) {
			for(int i = 0; i < 6; i++) {
				if(allJoints[i] != null && !allJoints[i].Equals(null) && allJoints[i].connectedBody != null) {
					allJoints[i].connectedBody.gameObject.layer = LayerMask.NameToLayer("Default");
					Destroy(allJoints[i]);
				}
			}
			Destroy(gameObject);
			jointShatter();
			reportDestroyedToParent();
		}
	}

	public void jointShatter() {
		//instantiate 8 smaller cubes in same location
		GameObject[] shards = new GameObject[8];
		for (int i = 0; i < 8; i++) {
			shards[i] = Instantiate(jointShard);
			shards[i].transform.parent = gameObject.transform.parent;
			shards[i].transform.localPosition = gameObject.transform.localPosition;
			shards[i].transform.localRotation = gameObject.transform.localRotation;
		}
		
		shards[0].transform.localPosition += new Vector3(1.0f,1.0f,1.0f) * shards[0].transform.localScale.x;
		shards[1].transform.localPosition += new Vector3(-1.0f,1.0f,1.0f) * shards[1].transform.localScale.x;
		shards[2].transform.localPosition += new Vector3(1.0f,-1.0f,1.0f) * shards[2].transform.localScale.x;
		shards[3].transform.localPosition += new Vector3(-1.0f,-1.0f,1.0f) * shards[3].transform.localScale.x;
		shards[4].transform.localPosition += new Vector3(1.0f,1.0f,-1.0f) * shards[4].transform.localScale.x;
		shards[5].transform.localPosition += new Vector3(-1.0f,1.0f,-1.0f) * shards[5].transform.localScale.x;
		shards[6].transform.localPosition += new Vector3(1.0f,-1.0f,-1.0f) * shards[6].transform.localScale.x;
		shards[7].transform.localPosition += new Vector3(-1.0f,-1.0f,-1.0f) * shards[7].transform.localScale.x;

	}

	public void reportDestroyedToParent() {
		if(transform.parent.gameObject.GetComponent<structure_behavior>()) {
			transform.parent.gameObject.GetComponent<structure_behavior>().reportDestroyed();
		}
	}
}
