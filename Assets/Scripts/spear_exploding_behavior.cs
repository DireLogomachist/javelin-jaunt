﻿using UnityEngine;
using System.Collections;


public class spear_exploding_behavior : spear_behavior {

	public float power = 200.0f;
	public float radius = 3.0f;
	public float upMod = 0.01f;

	bool exploded = false;

	GameObject explObject;
	ParticleSystem explosion;

	public override void Start() {
		base.Start();
		explObject = transform.FindChild("spear_explosion").gameObject;
		explosion = explObject.GetComponent<ParticleSystem>();
	}

	void OnCollisionEnter(Collision c) {
		//Debug.Log("Hit something");
		if(!exploded) {
			exploded = true;
			
			GetComponent<Renderer>().enabled = false;
			GetComponent<BoxCollider>().enabled = false;
			GetComponent<Rigidbody>().velocity = Vector3.zero;
			GetComponent<Rigidbody>().isKinematic = true;

			StartCoroutine(delayedExplosion(c));
		} else {
			Debug.Log("Already sploded...");
		}
	}


	IEnumerator delayedExplosion(Collision c) {
		Vector3 explosionPos = c.contacts[0].point;

		yield return new WaitForSeconds(0.2f);

		explosion.Play();

		Collider[] colliders = Physics.OverlapSphere(explosionPos,radius);
		foreach(Collider hit in colliders) {
			Rigidbody rb = hit.GetComponent<Rigidbody>();

			if(rb != null) {
				rb.AddExplosionForce(power,explosionPos,radius,upMod, ForceMode.Impulse);
			}
		}

	}
}
