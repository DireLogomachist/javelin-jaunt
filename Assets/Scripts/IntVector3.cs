﻿using UnityEngine;

public class IntVector3 {
	public int x { get; private set;}
	public int y { get; private set;}
	public int z { get; private set;}

	public IntVector3() {x = y = z = 0;}

	public IntVector3(int x,int y,int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public IntVector3(IntVector3 input) {
		this.x = input.x;
		this.y = input.y;
		this.z = input.z;
	}

	public IntVector3(Vector3 input) {
		x = (int)Mathf.Round(input.x);
		y = (int)Mathf.Round(input.y);
		z = (int)Mathf.Round(input.z);
	}

	public static IntVector3 operator +(IntVector3 left, IntVector3 right) {
		return new IntVector3(left.x + right.x, left.y + right.y, left.z + right.z);
	}

	public static IntVector3 operator -(IntVector3 left, IntVector3 right) {
		return new IntVector3(left.x - right.x, left.y - right.y, left.z - right.z);
	}

	public static IntVector3 operator -(IntVector3 left) {
		return new IntVector3(-left.x, -left.y, -left.z);
	}

	public static IntVector3 operator *(IntVector3 left, IntVector3 right) {
		return new IntVector3(left.x * right.x, left.y * right.y, left.z * right.z);
	}

	public static IntVector3 operator *(IntVector3 left, int right) {
		return new IntVector3(left.x * right, left.y * right, left.z * right);
	}

	public static IntVector3 operator /(IntVector3 left, IntVector3 right) {
		return new IntVector3(left.x / right.x, left.y / right.y, left.z / right.z);
	}
	
	public static bool operator ==(IntVector3 left, IntVector3 right) {
		if(ReferenceEquals(left,null)) {
			return ReferenceEquals(right,null);
		}
		return ((left.x == right.x) && (left.y == right.y) && (left.z == right.z));
	}

	public static bool operator !=(IntVector3 left, IntVector3 right) {
		if(ReferenceEquals(left,null)) {
			return !ReferenceEquals(right,null);
		}
		return !(left.x.Equals(right.x) && (left.y.Equals(right.y)) && (left.z.Equals(right.z)));
	}
	

	public override string ToString() {
		return "IntVector3(" + x + ", " + y + ", " + z + ")";
	}

	public Vector3 ToVector3() {
		return new Vector3(x, y, z);
	}

	public bool Equals(IntVector3 item) {
		return (this.x.Equals(item.x) && (this.y.Equals(item.y)) && (this.z.Equals(item.z)));
	}
	
	public override bool Equals(object obj) {
		//http://stackoverflow.com/questions/13470335
		var item = obj as IntVector3;
		if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
		if (obj.GetType() != this.GetType()) return false;

		return Equals((IntVector3)obj);
	}

	
	public override int GetHashCode() {
		//THANK YOU INTERNET RANDO
		//http://stackoverflow.com/questions/263400
		return new {x, y, z}.GetHashCode();
	}
	

}
