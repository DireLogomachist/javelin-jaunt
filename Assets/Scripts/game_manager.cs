﻿using UnityEngine;
using System.Collections;

public class game_manager : MonoBehaviour {
	private static game_manager _instance;
	public static game_manager Instance { get { return _instance; } }

	public int levelMax = 3;
	public int levelIndex = 0;
	public GameObject[] levels;
	public bool pastIntro = false;

	
	public void Awake() {
		if (_instance != null && _instance != this) {
			Destroy(this.gameObject);
		} else {
			_instance = this;
		}
	}

	void Start() {
		levels[0].transform.GetChild(0).GetComponent<structure_behavior>().enableDestruction();
	}
	
	void Update() {
		if(Input.GetKeyDown("space") && !pastIntro) {
			Camera.main.GetComponent<Animator>().Play("start");
			StartCoroutine(delayEnable());
		}
	}

	public IEnumerator nextLevel() {
		level_markers.Instance.levelCompleteUI(levelIndex);
		levelIndex++;
		yield return new WaitForSeconds(1.0f);
		Vector3 newPos = player.Instance.transform.position;
		newPos.y = newPos.y - 75f;

		if(levelIndex < levelMax) {
			StartCoroutine(smoothLerp(player.Instance.gameObject,newPos,2.0f));
			StartCoroutine(delayEnableDestruction());
		} else {
			// Credits trigger
			GameObject credits = GameObject.Find("credits");
			credits.GetComponent<MeshRenderer>().enabled = true;
			Vector3 creditsPos = credits.transform.position;
			creditsPos.y = creditsPos.y - 5.0f;
			StartCoroutine(smoothLerp(credits, creditsPos, 2.0f));
		}
	}

	IEnumerator delayEnable() {
		yield return new WaitForSeconds(3.0f);
		level_markers.Instance.showLevelUI();
		pastIntro = true;
	}

	IEnumerator delayEnableDestruction() {
		yield return new WaitForSeconds(2.5f);
		GameObject lvl = levels[levelIndex];
		lvl.transform.GetChild(0).GetComponent<structure_behavior>().enableDestruction();
	}

	IEnumerator smoothLerp(GameObject obj, Vector3 pos, float time) {
		float elapsed = 0;
		Vector3 startPos = obj.transform.position;
		while(elapsed < time) {
			float t = elapsed/time;
			obj.transform.position = Vector3.Lerp(startPos, pos, t*t * (3f - 2f*t));
			elapsed += Time.deltaTime;
			yield return null;
		}
	}

}
