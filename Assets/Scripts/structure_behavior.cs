﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class structure_behavior : MonoBehaviour {
	public string inputFile;

	public GameObject bracePrefab, jointPrefab;

	public int width = 1, height = 1, depth = 1;
	public int braceSize = 8;
	public int jointSize = 4;
	public float jointOffset = 0.1f; //uh, doesn't seem to do anything hmm
	Vector3 structOffset;

	int health, maxHealth;
	bool destructing = false;
	float verticalBreakIncreaseFactor = 1.25f;
	
	GameObject[,,] joints;
	depth_marker depthMarker;

	IntVector3[] cubeCoords = new IntVector3[8];
	IntVector3[] adjacentCoords = new IntVector3[6];
	
	void Start() {
		health = maxHealth = 4 * width * height * depth;
		joints = new GameObject[0,0,0];
		depthMarker = transform.FindChild("depth_marker").GetComponent<depth_marker>();

		int counter = 0;
		for(int i = 0; i < 2; i++) {for(int j = 0; j < 2; j++) {for(int k = 0; k < 2; k++) {
			cubeCoords[counter] = new IntVector3(i, j, k); counter++;
		}}}

		adjacentCoords[0] = new IntVector3(-1,0,0);
		adjacentCoords[1] = new IntVector3(1,0,0);
		adjacentCoords[2] = new IntVector3(0,-1,0);
		adjacentCoords[3] = new IntVector3(0,1,0);
		adjacentCoords[4] = new IntVector3(0,0,-1);
		adjacentCoords[5] = new IntVector3(0,0,1);

		if(inputFile == "") {
			generateStructure();
		} else {
			generateStructureFromJSON();
		}

		if(gameObject.transform.GetChild(0) != null) {
			GameObject.Destroy(transform.FindChild("marker").gameObject);
		}
	}

	void Update() {
		foreach(GameObject joint in joints) {
			if(joint != null && joint.transform.position.y < depthMarker.getDepthBoundary()) {
				joint.GetComponent<struct_joint>().selfDestruct();
			}
		}
	}

	void generateStructure() {
		//create structure starting with joints array
		joints = new GameObject[width + 1, height, depth + 1];
		
		structOffset = new Vector3((braceSize + jointSize*2 + jointSize*2*jointOffset) * (1 - .5f*width) * .5f, 0, (braceSize + jointSize*2 + jointSize*2*jointOffset) * (1 - .5f*depth) * .5f);

		//x-axis is i, y-axis is j, z-axis is k
		for(int i = 0; i < width + 1; i++) {
			for(int j = 0; j < height; j++) {
				for(int k = 0; k < depth + 1; k++) {
					//create leg braces and connecting braces
					//link the braces and the nodes
					GameObject joint = GameObject.Instantiate(jointPrefab);
					joint.transform.parent = gameObject.transform;
					joint.transform.rotation = gameObject.transform.rotation;
					
					joint.transform.localPosition = new Vector3((braceSize + jointSize*2*jointOffset) * (i - 1), (braceSize + jointSize*2*jointOffset) * j, (braceSize + jointSize*2*jointOffset) * (k - 1)) + structOffset;

					struct_joint joint_script = joint.GetComponent<struct_joint>();
					joint_script.setIndex(new Vector3(i,j,k));
					
					//each joint only ever creates braces in 3 directions, but will check their opposites to check for existing braces to connect to
					GameObject up_brace;
					GameObject right_brace;
					GameObject forward_brace;
					
					//create braces and add as configJoints
					if(i < width) {
						right_brace = GameObject.Instantiate(bracePrefab);
						right_brace.GetComponent<brace_behavior>().numConnections++;
						right_brace.transform.parent = gameObject.transform;
						right_brace.transform.localPosition = joint.transform.localPosition + new Vector3(braceSize*1/2 + jointOffset*jointSize,0,0);
						right_brace.transform.rotation = right_brace.transform.parent.rotation;
						right_brace.transform.Rotate(90.0f*Vector3.up, Space.Self);
						randomRotate(right_brace);
						joint_script.right.connectedBody = right_brace.GetComponent<Rigidbody>();
						joint_script.enableConfigJoint(joint_script.right);
					}
					if(j < height-1) {
						up_brace = GameObject.Instantiate(bracePrefab);
						up_brace.GetComponent<brace_behavior>().numConnections++;
						up_brace.transform.parent = gameObject.transform;
						up_brace.transform.localPosition = joint.transform.localPosition + new Vector3(0, braceSize*1/2 + jointOffset*jointSize, 0);
						up_brace.transform.rotation = up_brace.transform.parent.rotation;
						up_brace.transform.Rotate(90.0f*Vector3.right, Space.Self);
						randomRotate(up_brace);
						joint_script.up.connectedBody = up_brace.GetComponent<Rigidbody>();
						joint_script.up.breakForce = joint_script.up.breakForce * verticalBreakIncreaseFactor;
						joint_script.up.breakTorque = joint_script.up.breakTorque * verticalBreakIncreaseFactor;
						joint_script.enableConfigJoint(joint_script.up);
					}
					if(k < depth) {
						forward_brace = GameObject.Instantiate(bracePrefab);
						forward_brace.GetComponent<brace_behavior>().numConnections++;
						forward_brace.transform.parent = gameObject.transform;
						forward_brace.transform.localPosition = joint.transform.localPosition + new Vector3(0,0,braceSize*1/2 + jointOffset*jointSize);
						forward_brace.transform.rotation = forward_brace.transform.parent.rotation;
						randomRotate(forward_brace);
						joint_script.forward.connectedBody = forward_brace.GetComponent<Rigidbody>();
						joint_script.enableConfigJoint(joint_script.forward);
					}
					
					//check for existing braces on previous joints
					if(i > 0 && joints[i-1,j,k].GetComponent<struct_joint>().right != null) {//check for left brace
						joint_script.left.connectedBody = joints[i-1,j,k].GetComponent<struct_joint>().right.connectedBody.GetComponent<Rigidbody>();
						joints[i-1,j,k].GetComponent<struct_joint>().right.connectedBody.gameObject.GetComponent<brace_behavior>().numConnections++;
						joint_script.enableConfigJoint(joint_script.left);
					}
					if(j > 0 && joints[i,j-1,k].GetComponent<struct_joint>().up != null) {//check for down brace
						joint_script.down.connectedBody = joints[i,j-1,k].GetComponent<struct_joint>().up.connectedBody.GetComponent<Rigidbody>();
						joints[i,j-1,k].GetComponent<struct_joint>().up.connectedBody.gameObject.GetComponent<brace_behavior>().numConnections++;
						joint_script.enableConfigJoint(joint_script.down);
					}
					if(k > 0 && joints[i,j,k-1].GetComponent<struct_joint>().forward != null) {//check for back brace
						joint_script.back.connectedBody = joints[i,j,k-1].GetComponent<struct_joint>().forward.connectedBody.GetComponent<Rigidbody>();
						joints[i,j,k-1].GetComponent<struct_joint>().forward.connectedBody.gameObject.GetComponent<brace_behavior>().numConnections++;
						joint_script.enableConfigJoint(joint_script.back);
					}

					//create legs on lowest level
					if(j == 0) {
						GameObject down_brace = GameObject.Instantiate(bracePrefab);
						down_brace.GetComponent<brace_behavior>().numConnections++;
						down_brace.transform.parent = gameObject.transform;
						down_brace.transform.localPosition = joint.transform.localPosition + new Vector3(0, -braceSize*1/2 - jointOffset*jointSize, 0);
						down_brace.transform.rotation = down_brace.transform.parent.rotation;
						down_brace.transform.Rotate(90.0f*Vector3.right, Space.Self);
						randomRotate(down_brace);
						joint_script.down.connectedBody = down_brace.GetComponent<Rigidbody>();
						joint_script.down.breakForce = joint_script.down.breakForce * verticalBreakIncreaseFactor;
						joint_script.down.breakTorque = joint_script.down.breakTorque * verticalBreakIncreaseFactor;
						joint_script.enableConfigJoint(joint_script.down);
					}

					//delete configJoints without a connectBody
					foreach(ConfigurableJoint configJoint in joint_script.allJoints) {
						if(configJoint.connectedBody == null) {
							Destroy(configJoint);
						}
					}
					joints[i,j,k] = joint;
				}
			}
		}
	}

	void generateStructureFromJSON() {
		Structure jsonStructure = JsonUtility.FromJson<Structure>(Resources.Load<TextAsset>(inputFile).text);

		int maxBlocksX, maxBlocksY, maxBlocksZ;
		List<Vector3> blockList = new List<Vector3>();
		health = maxHealth = 0;

		for(int i = 0; i < jsonStructure.blocks.Length; i++) {
			blockList.Add(new Vector3(jsonStructure.blocks[i].x,jsonStructure.blocks[i].y,jsonStructure.blocks[i].z));
		}

		maxBlocksX = (int)Mathf.Round(blockList.Max(vec => vec.x)) + 1;
		maxBlocksY = (int)Mathf.Round(blockList.Max(vec => vec.y)) + 1;
		maxBlocksZ = (int)Mathf.Round(blockList.Max(vec => vec.z)) + 1;

		width = maxBlocksX; height = maxBlocksY; depth = maxBlocksZ; //unproven on reportDestroyed()

		structOffset = new Vector3((braceSize + jointSize*2 + jointSize*2*jointOffset) * (1 - .5f*maxBlocksX) * .5f, 0, (braceSize + jointSize*2 + jointSize*2*jointOffset) * (1 - .5f*maxBlocksZ) * .5f);

		blockList.OrderBy(vec => vec.z).ThenBy(vec => vec.y).ThenBy(vec => vec.x);
		
		joints = new GameObject[maxBlocksX + 1, maxBlocksY + 1, maxBlocksZ + 1];
		Block[,,] blockArray = new Block[maxBlocksX, maxBlocksY, maxBlocksZ];

		for(int i = 0; i < blockList.Count; i++) {
			IntVector3 basePos = new IntVector3(blockList[i]);

			IntVector3[] posList = new IntVector3[8];
			for(int x = 0; x < 8; x++) {
				posList[x] = basePos + cubeCoords[x];
			}
			
			Block block = new Block();
			blockArray[basePos.x, basePos.y, basePos.z] = block;

			foreach(IntVector3 pos in posList) {
				if(joints[pos.x,pos.y,pos.z] == null && pos.y != 0) {
					GameObject joint = createJoint(pos);
					maxHealth++;
					
					joints[pos.x,pos.y,pos.z] = joint;
					block.joints[pos.x-basePos.x,pos.y-basePos.y,pos.z-basePos.z] = joint;
					block.jointDict[new IntVector3(pos.x,pos.y,pos.z)] = joint;
				}
			}
		}

		
		for(int i = 0; i < blockArray.GetLength(0); i++) {
			for(int j = 0; j < blockArray.GetLength(1); j++) {
				for(int k = 0; k < blockArray.GetLength(2); k++) {
					if(blockArray[i,j,k] == null) continue;
					Block block = blockArray[i,j,k];

					//gather list of adjacent blocks (and self)
					Block[] adjacent = new Block[9];
					for(int x = 0; x < 8; x++) {
						//check for missing adjacent
						if(i + cubeCoords[x].x >= blockArray.GetLength(0) || j + cubeCoords[x].y >= blockArray.GetLength(1) || k + cubeCoords[x].z >= blockArray.GetLength(2)) {
							adjacent[x] = null;
						} else {
							adjacent[x] = blockArray[i + cubeCoords[x].x, j + cubeCoords[x].y, k + cubeCoords[x].z];
						}
					}
					adjacent[8] = block;
					
					foreach(KeyValuePair<IntVector3, GameObject> entry in block.jointDict) {
						GameObject joint = entry.Value;
						IntVector3 pos = entry.Key;
						struct_joint jointScript = joint.GetComponent<struct_joint>();
						jointScript.setIndex(pos);

						//loop through adjacent (and self) blocks looking for 6 shared joints
						for(int x = 0; x < adjacent.Length; x++) {
							Block adjBlock = adjacent[x];
							if(adjBlock == null) continue;

							//loop through list of pos + adjacentCoords
							for(int y = 0; y < adjacentCoords.Length; y++) {
								IntVector3 connectPos = pos + adjacentCoords[y];
								
								if(adjBlock.jointDict.ContainsKey(connectPos)) {
									//found adjacent joint, create brace and connect
									GameObject oppJoint = adjBlock.jointDict[connectPos];
									struct_joint oppJointScript = oppJoint.GetComponent<struct_joint>();
									IntVector3 connectDir = connectPos - pos;

									ConfigurableJoint configJoint = jointScript.selectConfigJoint(connectDir);
									ConfigurableJoint oppConfigJoint = oppJointScript.selectConfigJoint(-connectDir);

									if(oppConfigJoint != null && oppConfigJoint.connectedBody != null) continue;

									GameObject brace = GameObject.Instantiate(bracePrefab);
									brace.GetComponent<brace_behavior>().numConnections++;
									brace.transform.parent = gameObject.transform;
									brace.transform.rotation = brace.transform.parent.rotation;
									brace.transform.localPosition = (joint.transform.localPosition + oppJoint.transform.localPosition)/2.0f;
									
									Vector3 angle = new IntVector3(connectDir * 90).ToVector3();
									angle = new Vector3(angle.y, angle.x, angle.z);
									brace.transform.Rotate(angle, Space.Self);
									randomRotate(brace);
									
									configJoint.connectedBody = brace.GetComponent<Rigidbody>();
									oppConfigJoint.connectedBody = brace.GetComponent<Rigidbody>();
									
									if(connectDir == new IntVector3(0,1,0) || connectDir == new IntVector3(0,-1,0)) {
										configJoint.breakForce *= verticalBreakIncreaseFactor;
										configJoint.breakTorque *= verticalBreakIncreaseFactor;
										oppConfigJoint.breakForce *= verticalBreakIncreaseFactor;
										oppConfigJoint.breakTorque *= verticalBreakIncreaseFactor;
									}

									jointScript.enableConfigJoint(configJoint);
									oppJointScript.enableConfigJoint(oppConfigJoint);
								}
							}
						}
					}
				}
			}
		}

		//create legs on lowest level
		for(int i = 0; i < joints.GetLength(0); i++) {
			for(int k = 0; k < joints.GetLength(2); k++) { 
				if(joints[i,1,k] == null) continue;

				GameObject joint = joints[i,1,k];
				struct_joint joint_script = joint.GetComponent<struct_joint>();

				GameObject down_brace = GameObject.Instantiate(bracePrefab);
				down_brace.GetComponent<brace_behavior>().numConnections++;
				down_brace.transform.parent = gameObject.transform;
				down_brace.transform.localPosition = joint.transform.localPosition + new Vector3(0, -braceSize*1/2 - jointOffset*jointSize, 0);
				down_brace.transform.rotation = down_brace.transform.parent.rotation;
				down_brace.transform.Rotate(90.0f*Vector3.right, Space.Self);
				randomRotate(down_brace);

				ConfigurableJoint configJoint = joint_script.selectConfigJoint(new IntVector3(0,-1,0));
				configJoint.connectedBody = down_brace.GetComponent<Rigidbody>();
				configJoint.breakForce *= verticalBreakIncreaseFactor;
				configJoint.breakTorque *= verticalBreakIncreaseFactor;
				joint_script.enableConfigJoint(configJoint);
			}
		}

		health = maxHealth;
	}

	GameObject createJoint(IntVector3 pos) {
		GameObject joint = GameObject.Instantiate(jointPrefab);
		joint.transform.parent = gameObject.transform;
		joint.transform.rotation = gameObject.transform.rotation;
		joint.transform.localPosition = new Vector3((braceSize + jointSize*2*jointOffset) * (pos.x - 1), (braceSize + jointSize*2*jointOffset) * (pos.y - 1), (braceSize + jointSize*2*jointOffset) * (pos.z - 1)) + structOffset;
		joint.GetComponent<struct_joint>().setIndex(pos);
		return joint;
	}

	void randomRotate(GameObject brace) {
		int rand = Random.Range(0,4);
		int randFlip = Random.Range(0,2);
		Vector3 rot = (randFlip*180.0f)*Vector3.right + rand*90.0f*Vector3.forward;
		brace.transform.Rotate(rot, Space.Self);
	}

	void selfDestruct() {
		destructing = true;
		//plan: destroy the structure from the ground up
		//random schedule destruction starting from bottom row
		//poll rotation of joints - if enough average outside of vertical, then self destruct

		//currently just destroys everything at once with random delay
		for(int i = 0; i < width + 1; i++) {
			for(int j = 0; j < height + 1; j++) {
				for(int k = 0; k < depth + 1; k++) {
					if(joints[i,j,k] != null) {
						StartCoroutine(delayDestruct(joints[i,j,k]));
					} 
				}
			}
		}
	}

	IEnumerator delayDestruct(GameObject joint) {
		float wait = Random.Range(1.0f, 5.0f);
		yield return new WaitForSeconds(wait);
		if(joint != null) {
			joint.GetComponent<struct_joint>().selfDestruct();
		}
	}

	IEnumerator delayedLoadNextLevel() {
		yield return new WaitForSeconds(2.0f);
		StartCoroutine(game_manager.Instance.nextLevel());
	}

	public void reportDestroyed() {
		health--;
		float status = (float)health/(float)maxHealth;
		if(status < .4f  && !destructing) {
			selfDestruct();
		}
		if(status < .01f) {
			StartCoroutine(delayedLoadNextLevel());
		}
	}

	public void disableDestruction() {
		for(int i = 0; i < width + 1; i++) {
			for(int j = 0; j < height + 1; j++) {
				for(int k = 0; k < depth + 1; k++) {
					if(joints[i,j,k] != null) {
						joints[i,j,k].GetComponent<struct_joint>().destructEnabled = false;
					}
				}
			}
		}
	}

	public void enableDestruction() {
		for(int i = 0; i < width + 1; i++) {
			for(int j = 0; j < height + 1; j++) {
				for(int k = 0; k < depth + 1; k++) {
					if(joints[i,j,k] != null) {
						joints[i,j,k].GetComponent<struct_joint>().destructEnabled = true;
					} 
				}
			}
		}
	}
}


public class Block {
	public GameObject[,,] joints;
	public GameObject[,,] braces;

	public Dictionary<IntVector3, GameObject> jointDict;

	public Block() {
		joints = new GameObject[2,2,2];
		braces = new GameObject[2,2,3];
		jointDict = new Dictionary<IntVector3, GameObject>();
	}
}
