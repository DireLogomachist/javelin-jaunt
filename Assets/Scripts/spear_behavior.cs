﻿using UnityEngine;
using System.Collections;

public class spear_behavior : MonoBehaviour {
   
    public float throwStrength = 700; //deprecated...
	public float throwVelocity = 70;
    public float spinStrength = 20;
	public float gravityWait = 0.75f;

	[HideInInspector]
    public bool thrown = false;
	[HideInInspector]
	public bool hit = false;

    new Rigidbody rigidbody;
    Transform direction;

	public virtual void Start () {
	    rigidbody = gameObject.GetComponent<Rigidbody>();
        direction = gameObject.transform;
	}
	
	public virtual void Update () {
		if(!thrown) {
			Vector3 normalMouse = Input.mousePosition - new Vector3(Screen.width/2, Screen.height/2, 0);
			normalMouse = new Vector3(normalMouse.z, normalMouse.y, normalMouse.x);
			Vector3 point_pos = Camera.main.gameObject.transform.position + normalMouse/10 + Camera.main.gameObject.transform.forward * 40;
			transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, point_pos - transform.position, 5*Time.deltaTime, 0.0f));
		} else if(thrown && rigidbody.velocity.magnitude > 15) {
			//trajectory arc rotation limited on velocity - otherwise they leap like salmon
			transform.rotation = Quaternion.LookRotation(rigidbody.velocity);
		}
	}

	void OnCollisionEnter(Collision c) {
		hit = true;
		rigidbody.useGravity = true;
	}

    public void fireSpear() {
		thrown = true;
        rigidbody.isKinematic = false;
		rigidbody.AddForce(direction.forward * throwVelocity, ForceMode.VelocityChange);
        rigidbody.AddTorque(direction.forward * spinStrength, ForceMode.Impulse);
		//StartCoroutine(waitForGravity());
    }

	IEnumerator waitForGravity() {
		yield return new WaitForSeconds(gravityWait);
		rigidbody.useGravity = true;
	}

	
}
