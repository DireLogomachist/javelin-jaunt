﻿using UnityEngine;
using System.Collections;

public class hover : MonoBehaviour {
	
	public float amplitude = 0.03f;
	public float frequency = 0.1f;

	Vector3 pos;

	void Start() {
		pos = gameObject.transform.localPosition;
	}

	void Update () {
		transform.localPosition = pos + Vector3.up * amplitude * Mathf.Sin(2*Mathf.PI*frequency*Time.time);
	}
}
