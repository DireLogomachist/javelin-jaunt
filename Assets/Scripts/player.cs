﻿using UnityEngine;
using System.Collections;

public class player : MonoBehaviour {
	private static player _instance;
    public static player Instance { get { return _instance; } }

    public GameObject spear_prefab;
    public GameObject spear_placeholder;
	public float spear_cooldown_time = 1;

	GameObject queued_spear;
	public bool can_throw = true;


	public void Awake() {
		//Singleton pattern
		if (_instance != null && _instance != this) {
			Destroy(this.gameObject);
		} else {
			_instance = this;
		}
	}


	void Start() {
		loadSpear();
	}
	
	void Update() {
	}

	void loadSpear() {
		if(queued_spear == null || queued_spear.GetComponent<spear_behavior>().thrown == true) {
			queued_spear = GameObject.Instantiate(spear_prefab);
			queued_spear.transform.parent = gameObject.transform;
			queued_spear.transform.localPosition = spear_placeholder.transform.localPosition;
			queued_spear.transform.localRotation = spear_placeholder.transform.localRotation;
			queued_spear.transform.GetChild(0).GetComponent<TrailRenderer>().enabled = false;
			//line up to cursor via raycast maybe?
		}
	}

	public void throwSpear(Vector3 target) {
		if(can_throw && game_manager.Instance.pastIntro) {
			//adjust spear rotation to account for gravity drop
			float spear_speed = queued_spear.GetComponent<spear_behavior>().throwVelocity;
			float throw_dist = Vector3.Magnitude(target - queued_spear.transform.position);
			float airtime = throw_dist / spear_speed;
			float vertical_adjust = .5f * Physics.gravity.magnitude * Mathf.Pow(airtime, 2);

			target.y += vertical_adjust;

			queued_spear.transform.LookAt(target, Vector3.up);
			queued_spear.transform.GetChild(0).GetComponent<TrailRenderer>().enabled = true;
			queued_spear.GetComponent<spear_behavior>().fireSpear();
			StartCoroutine(spearCooldown());
		}
	}

	IEnumerator spearCooldown() {
		can_throw = false;
		yield return new WaitForSeconds(spear_cooldown_time);
		loadSpear();
		can_throw = true;
	}
}
