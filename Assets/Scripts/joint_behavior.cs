﻿using UnityEngine;
using System.Collections;

public class joint_behavior : MonoBehaviour {
	public float hitFlashTimer = 0.5f;

	Renderer rend;
	Color currentColor;

	void Start () {
		rend = gameObject.GetComponent<Renderer>();
		currentColor = rend.material.color;
	}

	void Update () {}

	void OnCollisionEnter(Collision c) {
		if(c.collider.tag == "Projectile") {
			Destroy(gameObject);
			//StartCoroutine(colorChange());
		}
	}

    IEnumerator colorChange() {
		rend.material.color = Color.red;
		yield return new WaitForSeconds(hitFlashTimer);
		rend.material.color = currentColor;
	}
}
