﻿using UnityEngine;
using System.Collections;
using System.Reflection;
using System;

public static class utilities {

	public static T GetCopyOf<T>(this Component comp, T other) where T : Component {
		Type type = comp.GetType();
		if (type != other.GetType()) return null; // type mis-match
		BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
		PropertyInfo[] pinfos = type.GetProperties(flags);
		foreach (var pinfo in pinfos) {
			if (pinfo.CanWrite) {
				try {
					pinfo.SetValue(comp, pinfo.GetValue(other, null), null);
				}
				catch { } // In case of NotImplementedException being thrown. For some reason specifying that exception didn't seem to catch it, so I didn't catch anything specific.
			}
		}
		FieldInfo[] finfos = type.GetFields(flags);
		foreach (var finfo in finfos) {
			finfo.SetValue(comp, finfo.GetValue(other));
		}
		return comp as T;
	}



	public static void CopyComponent(Component component, GameObject target) {
		Type type = component.GetType();
		target.AddComponent(type);

		var latestComp = target.GetComponents(type)[target.GetComponents(type).Length-1];

		//BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic |  BindingFlags.Default | BindingFlags.Instance | BindingFlags.FlattenHierarchy; //BindingFlags.DeclaredOnly
		BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
		PropertyInfo[] propInfo = type.GetProperties(flags);
		foreach (var property in propInfo)
		{
			if (property.CanWrite) {
				try {
					//property.SetValue(target.GetComponent(type), property.GetValue(component, null), null);
					property.SetValue(latestComp, property.GetValue(component, null), null);
				}
				catch {Debug.Log("Anything?");};
			}
		}
		FieldInfo[] finfos = type.GetFields(flags);
		foreach (var finfo in finfos) {
			//finfo.SetValue(target.GetComponent(type), finfo.GetValue(component));
			finfo.SetValue(latestComp, finfo.GetValue(component));
		}
	}

}
