﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Structure {

	public string name;
	public int level;

	public triplet[] blocks;

	public braceProperty[] braceProperties;
	public jointProperty[] jointProperties;

}

[System.Serializable]
public struct triplet {
	public int x;
	public int y;
	public int z;
}

[System.Serializable]
public struct tuple {
	public int x;
	public int y;
}

[System.Serializable]
public struct jointProperty {
	public triplet parentBlock;
	public tuple joint;
}

[System.Serializable]
public struct braceProperty {
	public triplet parentBlock;
	public tuple parentJoint;
	public jointDirection brace;
}

[System.Serializable]
public enum jointType {normal, exploding, unbreakable}

[System.Serializable]
public enum braceType {normal, breakable}

[System.Serializable]
public enum jointDirection {up, down, left, right, forward, back}