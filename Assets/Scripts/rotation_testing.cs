﻿using UnityEngine;
using System.Collections;

public class rotation_testing : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine(rotation());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator rotation() {
		yield return new WaitForSeconds(1.5f);
	
		//gameObject.transform.rotation = gameObject.transform.parent.rotation;
		gameObject.transform.rotation = GameObject.Find("parent").transform.rotation;

		yield return new WaitForSeconds(1.5f);

		gameObject.transform.Rotate(90.0f*Vector3.up, Space.Self);
		//gameObject.transform.right

		yield return new WaitForSeconds(1.5f);

		//randomRotate(gameObject);

		gameObject.transform.Rotate(90.0f*Vector3.right, Space.Self);

		//gameObject.transform.right

	}

	void randomRotate(GameObject brace) {
		int rand = Random.Range(0,4);
		int randFlip = Random.Range(0,2);
		//Vector3 rot = 45.0f*(randFlip*180.0f)*brace.transform.right + rand*90.0f*brace.transform.forward;
		Vector3 rot = -90.0f*brace.transform.right;
		brace.transform.Rotate(rot, Space.Self);
	}
}


