﻿using UnityEngine;
using System.Collections;

public class fade_in : MonoBehaviour {
	public float waitTime = 1.0f;
	public float fadeTime = 1.0f;

	void Start () {
		//GetComponent<Renderer>().enabled = false;
		StartCoroutine(fade());
	}
	
	void Update () {

	}

	IEnumerator timedFadeIn() {
		yield return new WaitForSeconds(waitTime);
		GetComponent<Renderer>().enabled = false;
	}

	IEnumerator fade() {
		float i = 0;
		MeshRenderer rend = gameObject.GetComponent<MeshRenderer>();
		rend.material.color = new Color(rend.material.color.r,rend.material.color.g,rend.material.color.b, 0);

		yield return new WaitForSeconds(waitTime);
		while(i < fadeTime) {
			i += Time.deltaTime;

			rend.material.color = new Color(rend.material.color.r,rend.material.color.g,rend.material.color.b,i/fadeTime);

			yield return null;
		}

	}
}
