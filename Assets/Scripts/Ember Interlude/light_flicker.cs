﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Light))]
public class light_flicker : MonoBehaviour {
	new Light light;
	public Light secondaryLight;

	public float minIntensity = 0.1f;
	public float maxIntensity = 5.0f;
	public float minWait = .1f;
	public float maxWait = .5f;

	public float movementFactor = 1.0f;
	public float rateFactor = 1.0f;
	public float waitFactor = 1.0f;

	Vector3 origPos;
	float origIntensity;
	
	//for lerp
	float random; 

	//for saw
	float wait= 0.5f;
	float rate = 0.0f;
	float counter = 0.0f;
	float intensity = 0.0f;


	void Start() {
		random = Random.Range(0.0f, 65535.0f);
		light = gameObject.GetComponent<Light>();
		intensity = light.intensity; //light.shadowStrength;
		wait = Random.Range(minWait, maxWait);

		origPos = transform.position;
		origIntensity = intensity;
	}
 
	void Update() {
		counter += Time.deltaTime;

		//LERP
		//float noise = Mathf.PerlinNoise(random, Time.time);
		//light.intensity = Mathf.Lerp(minIntensity*intensity, maxIntensity*intensity, noise);

		/*
		//Alternate
		if(counter >= wait) {
			counter = 0.0f;
			light.intensity = 1/(light.intensity*2);
		}
		*/

		//Saw wave
		if(counter >= wait) {
			//reset counter
			//randomize wait and intensity
			//calculate rate
			counter = 0.0f;
			wait = waitFactor*Random.Range(minWait, maxWait);
			intensity = Random.Range(0.0f, origIntensity);
			rate = rateFactor*intensity/wait;

			light.intensity = 1.0f;
			secondaryLight.intensity = 1.0f;
		} else {
			//increment/decrement intensity
			if(counter < wait/2) {
				light.intensity += rate*Time.deltaTime/3;
				secondaryLight.intensity += rate*Time.deltaTime/4;
				transform.position = origPos + new Vector3(0,rate*Time.deltaTime*movementFactor,0);
			} else {
				light.intensity -= rate*Time.deltaTime/3;
				secondaryLight.intensity -= rate*Time.deltaTime/4;
				transform.position = origPos - new Vector3(0,rate*Time.deltaTime*movementFactor,0);
			}
		}
		//Debug.Log(Time.deltaTime);
	}
}
