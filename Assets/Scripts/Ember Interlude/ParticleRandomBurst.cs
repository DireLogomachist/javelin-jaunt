﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(ParticleSystem))]
public class ParticleRandomBurst : MonoBehaviour {
	new ParticleSystem particleSystem;

	public Vector2 randomWait = new Vector2(0,1);
	public Vector4 spawnRect = new Vector4(-.75f,.75f,-.75f,.75f);
	public int burstCount = 30;

	float limit = 0;
	float timer = 0;

	Vector3 originalPos = new Vector3(0,0,0);
	Vector3 spawnPos = new Vector3(0,0,0);

	void Start () {
		particleSystem = GetComponent<ParticleSystem>();
		particleSystem.Stop();
		limit = Random.Range(randomWait.x, randomWait.y);

		originalPos = particleSystem.transform.localPosition;
		spawnPos = new Vector3(Random.Range(spawnRect.x, spawnRect.y), 0, Random.Range(spawnRect.x, spawnRect.y));
		particleSystem.transform.localPosition = originalPos + spawnPos;
	}

	void Update() {
		if(Time.time > 4.0f) {
			timer += Time.deltaTime;

			if(timer > limit) {
				spawnPos = new Vector3(Random.Range(spawnRect.x,spawnRect.y),Random.Range(spawnRect.z,spawnRect.w),0);
				particleSystem.transform.localPosition = spawnPos + originalPos;

				particleSystem.Emit(burstCount);
				timer = 0;
				limit = Random.Range(randomWait.x,randomWait.y);
			}
		}
	}
}
