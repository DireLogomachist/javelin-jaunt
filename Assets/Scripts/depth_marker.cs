﻿using UnityEngine;
using System.Collections;

public class depth_marker : MonoBehaviour {

	void Start() {
		//make object invisible
		if(gameObject.transform.GetChild(0) != null)
			Destroy(transform.GetChild(0).gameObject);
	}

	public float getDepthBoundary() {
		return transform.position.y;
	}
}
